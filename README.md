# Tea Selector

I can never decide which tea to have, and neither can my friends. So I made an app to select a tea for us to drink from my selection.

## Deployment
If you're using apache like me, you can configure your server to use `mod_wsgi` with this [guide](https://flask.palletsprojects.com/en/0.12.x/deploying/mod_wsgi/).

You should change the path in `tea_selector.wsgi` as needed.

