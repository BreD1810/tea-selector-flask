from flask import Flask, render_template, jsonify, request, flash, redirect, url_for
from tea_selector import app
from tea_selector.forms import *
from tea_selector.database_helper import *
from random import choice


@app.route("/")
def main_page():
    return render_template('home.html')


@app.route("/manage-tea")
def manage_tea():
    add_form = AddTeaForm()
    remove_form = RemoveTeaForm()
    return render_template('manage_tea.html', add_form=AddTeaForm(), remove_form=RemoveTeaForm.new())


@app.route("/add_tea", methods=['POST'])
def add_tea():
    response = request.form
    tea_added = add_tea_database(response)
    flash('Successfully added {}!'.format(tea_added))
    return redirect(url_for('manage_tea'))


@app.route("/remove_tea", methods=['POST'])
def remove_tea():
    response = request.form
    tea_deleted = delete_tea_from_database(response)
    flash('Successfully removed {}!'.format(tea_deleted))
    return redirect(url_for('manage_tea'))


@app.route("/get-tea", methods=['POST', 'GET'])
def get_tea():
    all_teas = get_all_teas()
    selected_tea = choice(all_teas)
    print(selected_tea)
    return jsonify({'tea_name': selected_tea.tea_name})


@app.errorhandler(404)
def error_404(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def error_500(error):
    return render_template('500.html'), 500
