from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import DataRequired
from tea_selector.database_helper import get_tea_selectfield


class AddTeaForm(FlaskForm):
    tea_name = StringField("Enter the name of the tea", validators=[DataRequired()])
    tea_type = SelectField("What is the type of the tea?",
                           choices=sorted([('', 'Select...'), ('b', 'Black Tea'), ('g', 'Green Tea'), ('w', 'White Tea'), ('h', 'Herbal Tea'), ('f', 'Fruit Tea'), ('r', 'Rooibos'),
                                           ('chinese', 'Chinese Tea'), ('chai', 'Chai Tea'), ('mint', 'Mint Tea'), ('matcha', 'Matcha')]),
                           validators=[DataRequired()])
    submit = SubmitField('Add Tea')


class RemoveTeaForm(FlaskForm):
    tea_name = SelectField("What tea do you want to delete?",
                           choices=[('', 'Select...')] + sorted(get_tea_selectfield()),
                           validators=[DataRequired()])
    submit = SubmitField('Remove Tea')

    @classmethod
    def new(cls):
        form = cls()
        form.tea_name.choices = [('', 'Select...')] + sorted(get_tea_selectfield())
        return form
