from tea_selector import db


class Tea(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tea_name = db.Column(db.String, nullable=False)
    tea_type = db.Column(db.String, nullable=False)

    def __repr__(self):
        return (f"Tea({self.tea_name}, {self.tea_type})")
