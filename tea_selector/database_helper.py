from tea_selector.models import *
from tea_selector import db


# Ensure the file is there
db.create_all()


def add_tea_database(response):
    tea_name = response['tea_name']
    tea_type = response['tea_type']
    tea = Tea(tea_name=tea_name, tea_type=tea_type)
    db.session.add(tea)
    db.session.commit()
    return tea_name


def delete_tea_from_database(response):
    tea_name = response['tea_name']
    print(tea_name)
    Tea.query.filter_by(tea_name=tea_name).delete()
    db.session.commit()
    return tea_name


def get_tea_selectfield():
    all_teas = Tea.query.all()
    return [(tea.tea_name, tea.tea_name) for tea in all_teas]


def get_all_teas():
    return Tea.query.all()
