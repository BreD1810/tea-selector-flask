from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import urandom

app = Flask(__name__)
# app.config["APPLICATION_ROOT"] = "/tea-selector"  # Needed for subpath on server
app.config['SECRET_KEY'] = urandom(16)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///teastore.db'

db = SQLAlchemy(app)


from tea_selector import routes
